package main

import (
	"fmt"
)

type TreeNode struct {
	Val         int
	Left, Right *TreeNode
}

func pathSum(root *TreeNode, targetSum int) (result [][]int) {
	if root == nil {
		return
	}
	if root.Left == nil && root.Right == nil {
		if root.Val == targetSum {
			result = [][]int{{targetSum}}
		}
		return
	}
	type List struct {
		TN           *TreeNode
		Parent, Next *List
		Sum          int
	}
	cur := &List{root, nil, nil, root.Val}
	next := cur
	for {
		if cur.TN.Left != nil {
			next.Next = &List{cur.TN.Left, cur, nil, cur.Sum + cur.TN.Left.Val}
			next = next.Next
			if next.Sum == targetSum && next.TN.Left == nil && next.TN.Right == nil {
				node := next
				var ints [1000]int
				i := 1000
				for node != nil {
					i--
					ints[i] = node.TN.Val
					node = node.Parent
				}
				result = append(result, ints[i:])
			}
		}
		if cur.TN.Right != nil {
			next.Next = &List{cur.TN.Right, cur, nil, cur.Sum + cur.TN.Right.Val}
			next = next.Next
			if next.Sum == targetSum && next.TN.Left == nil && next.TN.Right == nil {
				node := next
				var ints [1000]int
				i := 1000
				for node != nil {
					i--
					ints[i] = node.TN.Val
					node = node.Parent
				}
				result = append(result, ints[i:])
			}
		}
		cur = cur.Next
		if cur == next && cur.TN.Left == nil && cur.TN.Right == nil {
			return
		}

	}
}
func main() {
	input := &TreeNode{5,
		&TreeNode{4,
			&TreeNode{11,
				&TreeNode{7, nil, nil},
				&TreeNode{2, nil, nil},
			},
			nil},
		&TreeNode{8,
			&TreeNode{13, nil, nil},
			&TreeNode{4,
				&TreeNode{5, nil, nil},
				&TreeNode{1, nil, nil},
			},
		},
	}
	targetSum := 22
	fmt.Println(pathSum(input, targetSum)) //[[5,4,11,2],[5,8,4,5]]
	input = &TreeNode{1, &TreeNode{2, &TreeNode{3, &TreeNode{4, &TreeNode{5, nil, nil}, nil}, nil}, nil}, nil}
	targetSum = 15
	fmt.Println(pathSum(input, targetSum)) //[[1,2,3,4,5]]
}
